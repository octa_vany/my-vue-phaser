FROM node:16.6

ENV APP_ROOT /my-vue-phaser

RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}

COPY ./app ${APP_ROOT}
RUN npm install

RUN npm run generate
RUN npm run build

EXPOSE 5000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=5000

CMD ["npm", "start"]